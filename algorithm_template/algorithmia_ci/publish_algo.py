import Algorithmia


def publish_algo(mgmt_api_key, api_address, algo_schema, algo_name, algo_hash):
    client = Algorithmia.client(api_key=mgmt_api_key, api_address=api_address)

    algo = client.algo("{}/{}".format(algo_name, algo_hash))

    if algo_schema not in ["major", "minor", "revision"]:
        raise Exception("{} is not considered a valid algorithm version schema".format(algo_schema))
    print("--- releasing new {}".format(algo_schema))
    algo.publish(version_info={"version_type": algo_schema, "release_notes": "automatically deployed by CI"}, settings={"royalty_microcredits": 0}, details={"label": "CICD"})
