# Notebook Algorithm Deployer

This workflow allows you to automatically deploy
model artifacts generated from running a jupyter notebook to Algorithmia, backed by Gitlab.
This also enables automatic CI/CD and verification of the deployed model before promotion.

## To Config
clone the `credentials_example.json` file and reame it to `credentials.json`.
Make sure you fill in the necessary lines, if any have an `optional` key, you can omit those in your particular circumstance.

GITLAB_INSTANCE and GITLAB_RUNNER_ID are optional.

An example of a functional credentials file:
```json
{
 "API_ADDRESS": "https://api.algorithmia.com",
 "API_KEY": "API_KEY_HERE",
 "ALGORITHM_NAME": "fashion_mnist2",
 "GITLAB_REPOSITORY_OWNER": "james.sutton",
 "GITLAB_TOKEN": "GITLAB_TOKEN_HERE"
}
```

## To Run
Note the two jupyter notebooks in the `notebooks` directory.
if you want a simple "deploy to Algorithmia" notebook workflow,
use the just_deployment.ipynb notebook. You will need to provide a `model.t5` file in the root directory to deploy.

If you want to run the end-to-end demo including model training, use the full_demo.ipynb notebook.